package Adrian.Gora.Report.generator.service;

import Adrian.Gora.Report.generator.model.Order;
import Adrian.Gora.Report.generator.model.User;
import Adrian.Gora.Report.generator.service.Dto.CreateUpdateOrderDto;
import Adrian.Gora.Report.generator.service.Dto.OrderDto;
import Adrian.Gora.Report.generator.service.Dto.UserDto;
import Adrian.Gora.Report.generator.service.exception.UserDataInvalid;
import Adrian.Gora.Report.generator.service.mapper.OrderDtoMapper;
import Adrian.Gora.Report.generator.service.mapper.UserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {

    @Autowired
    private UserService userService;

    private final UserDtoMapper userDtoMapper;

    private final OrderDtoMapper orderDtoMapper;

    public List<OrderDto> getOrderList(String userId) {
        List<OrderDto> orderDtoList = new ArrayList();
        for (Order element : userService.findById(userId).getUserOrderList()) {
            orderDtoList.add(orderDtoMapper.orderToDto(element));
        }
        return orderDtoList;
    }


    public UserDto addOrderToUserList(List<CreateUpdateOrderDto> createUpdateOrderDtoList, HttpServletRequest request) {
        if (createUpdateOrderDtoList != null) {

            User user = userService.findById((String) request.getSession().getAttribute("userId"));

            for (CreateUpdateOrderDto element : createUpdateOrderDtoList) {

                Order order = new Order(element.getClientId(), element.getRequestId(), element.getName(), element.getQuantity(), element.getPrice());
                user.getUserOrderList().add(order);
            }
            return userDtoMapper.userToDto(user);
        }
        return null;
    }
}

