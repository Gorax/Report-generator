package Adrian.Gora.Report.generator.service;

import Adrian.Gora.Report.generator.service.Dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportService {

    @Autowired
    private OrderService orderService;

    public int numberOfAllClient(String userId) {
        int numberOfClient;

        numberOfClient = clientList(userId).size();
        return numberOfClient;
    }

    public int numberOfAllOrders(String userId) {
        return orderService.getOrderList(userId).size();
    }

    public double totalAmountOfOrders(String userId) {
        double totalAmount = 0;
        for (OrderDto element : orderService.getOrderList(userId)) {
            totalAmount += element.getPrice();
        }
        return totalAmount;
    }

    public double averageAmountOfAll(String userId) {
        return totalAmountOfOrders(userId) / numberOfAllOrders(userId);
    }

    public List<OrderDto> clientList(String userId) {
        List<OrderDto> temp = orderService.getOrderList(userId);
        List<OrderDto> clientList = new ArrayList<>();

        if (temp.isEmpty())
            return null;


        for (OrderDto element : temp) {
            if (clientList.stream().filter(e -> e.getClientId() == element.getClientId()).findAny().orElse(null) == null){
                clientList.add(element);
            }
        }
        return clientList;
    }

    public List<OrderDto> allOrdersList(String userId) {
        return orderService.getOrderList(userId);
    }

    public int numberOrderOfCustomerByIndex(String userId, int clientId) {
        List<OrderDto> temp = orderService.getOrderList(userId).stream().filter(e -> e.getClientId() == clientId).collect(Collectors.toList());
        if (temp.isEmpty()) {
            return -1;
        } else {
            return temp.size();
        }
    }

    public double totalAmountOfCustomerByIndexOrders(String userId, int clientId) {

        List<OrderDto> temp = orderService.getOrderList(userId).stream().filter(e -> e.getClientId() == clientId).collect(Collectors.toList());
        double totalAmout = 0;
        for (OrderDto element : temp) {
            totalAmout += element.getPrice();
        }
        return totalAmout;
    }

    public double averageAmountOfCustomer(String userId, Integer clientId) {
        if (clientId == null)
            return -1;

        return totalAmountOfCustomerByIndexOrders(userId, clientId) / numberOrderOfCustomerByIndex(userId, clientId);
    }

    public List<OrderDto> orderOfClientPerIndex(String userId, Integer clientId) {
        if (clientId == null)
            return null;

        List<OrderDto> temp = orderService.getOrderList(userId).stream().filter(e -> e.getClientId() == clientId).collect(Collectors.toList());
        return temp;
    }
}

