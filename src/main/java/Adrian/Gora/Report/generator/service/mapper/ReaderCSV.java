package Adrian.Gora.Report.generator.service.mapper;

import Adrian.Gora.Report.generator.service.Dto.CreateUpdateOrderDto;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReaderCSV {

    private static final String[] FILE_HEADER_MAPPING = {"ClientId", "RequestId", "Name", "Quantity", "Price"};

    private static final String ZAMOWIENIE_CLIENT_ID = "ClientId";
    private static final String ZAMOWIENIE_REQUEST_ID = "RequestId";
    private static final String ZAMOWIENIE_NAME = "Name";
    private static final String ZAMOWIENIE_QUANTITY = "Quantity";
    private static final String ZAMOWIENIE_PRICE = "Price";

    public static List<CreateUpdateOrderDto> readCsvFile(File file, HttpServletResponse response) throws IOException {

        FileReader fileReader = null;

        CSVParser csvFileParser = null;

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);

        List<CreateUpdateOrderDto> orderDtoList = new ArrayList<>();

        try {

            fileReader = new FileReader(file);

            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = csvRecords.get(i);
                CreateUpdateOrderDto createUpdateOrderDto = new CreateUpdateOrderDto(
                        Integer.parseInt(record.get(ZAMOWIENIE_CLIENT_ID)),
                        Long.parseLong(record.get(ZAMOWIENIE_REQUEST_ID)),
                        record.get(ZAMOWIENIE_NAME),
                        Integer.parseInt(record.get(ZAMOWIENIE_QUANTITY)),
                        Double.parseDouble(record.get(ZAMOWIENIE_PRICE)));

                orderDtoList.add(createUpdateOrderDto);
            }

            return orderDtoList;

        } catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();

        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing file/csvFileParser !!!");
                e.printStackTrace();
            }
        }
        return null;
    }
}
