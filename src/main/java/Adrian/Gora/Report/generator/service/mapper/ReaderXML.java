package Adrian.Gora.Report.generator.service.mapper;

import Adrian.Gora.Report.generator.service.Dto.CreateUpdateOrderDto;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ReaderXML {

    public static List<CreateUpdateOrderDto> ReadXmlFile(File file) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("request");

            List<CreateUpdateOrderDto> orderDtoList = new ArrayList<>();

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) nNode;


                    final String ZAMOWIENIE_CLIENT_ID = element.getElementsByTagName("clientId").item(0).getTextContent();
                    final String ZAMOWIENIE_REQUEST_ID = element.getElementsByTagName("requestId").item(0).getTextContent();
                    final String ZAMOWIENIE_NAME = element.getElementsByTagName("name").item(0).getTextContent();
                    final String ZAMOWIENIE_QUANTITY = element.getElementsByTagName("quantity").item(0).getTextContent();
                    final String ZAMOWIENIE_PRICE = element.getElementsByTagName("price").item(0).getTextContent();
                    CreateUpdateOrderDto createUpdateOrderDto = new CreateUpdateOrderDto(
                            Integer.parseInt(ZAMOWIENIE_CLIENT_ID),
                            Long.parseLong(ZAMOWIENIE_REQUEST_ID),
                            ZAMOWIENIE_NAME,
                            Integer.parseInt(ZAMOWIENIE_QUANTITY),
                            Double.parseDouble(ZAMOWIENIE_PRICE)
                    );
                    orderDtoList.add(createUpdateOrderDto);
                }
            }
            return orderDtoList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
