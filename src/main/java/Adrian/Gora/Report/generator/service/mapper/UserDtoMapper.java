package Adrian.Gora.Report.generator.service.mapper;

import Adrian.Gora.Report.generator.model.User;
import Adrian.Gora.Report.generator.service.Dto.CreateUpdateUserDto;
import Adrian.Gora.Report.generator.service.Dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserDtoMapper {

    public UserDto userToDto(User user) {
        return new UserDto(user.getId(), user.getName(), user.getRole());
    }

    public User toModel(CreateUpdateUserDto createUpdateUserDto) {
        return new User(UUID.randomUUID().toString(), createUpdateUserDto.getName(), "user", createUpdateUserDto.getUserOrderList());
    }
}
