package Adrian.Gora.Report.generator.view;

import Adrian.Gora.Report.generator.service.Dto.CreateUpdateUserDto;
import Adrian.Gora.Report.generator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.ArrayList;

@Controller
@Validated
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public ModelAndView registerView() {
        return new ModelAndView("login");
    }

    @PostMapping("/login")
    public void addUser(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String userName, HttpServletRequest request, HttpServletResponse response) throws IOException {

        userService.addUser(new CreateUpdateUserDto(userName, new ArrayList<>()), request);

        response.sendRedirect(response.encodeRedirectURL("/loadFile"));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict(Model model) {
        model.addAttribute("message", "invalid data");
        return new ModelAndView("/login");
    }
}
