package Adrian.Gora.Report.generator.view;

import Adrian.Gora.Report.generator.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/reportByIndex")
@Validated
public class ReportByIndex {

    @Autowired
    private ReportService reportService;

    @GetMapping
    public ModelAndView viewByIndex() {
        return new ModelAndView("/reportByIndex");
    }

    @PostMapping
    public ModelAndView reportByIndex(@Validated @NotNull Integer clientIndex, HttpServletRequest request, Model model) {
        if (reportService.numberOrderOfCustomerByIndex((String) request.getSession().getAttribute("userId"), clientIndex) == -1){
            model.addAttribute("message", "invalid client index!");
            return new ModelAndView("/reportByIndex");
        }else {
            model.addAttribute("numberOfOrders", reportService.numberOrderOfCustomerByIndex((String) request.getSession().getAttribute("userId"), clientIndex));
            model.addAttribute("totalAmount", reportService.totalAmountOfCustomerByIndexOrders((String) request.getSession().getAttribute("userId"), clientIndex));
            model.addAttribute("average", reportService.averageAmountOfCustomer((String) request.getSession().getAttribute("userId"), clientIndex));
            return new ModelAndView("/reportByIndex");
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody()
    public ModelAndView conflicts(Model model){
        model.addAttribute("message", "invalid client index!");
        return new ModelAndView("/reportByIndex");
    }
}
